DESCRIPTION = "SEB Multimedia Plateforme Test image"
LICENSE = "MIT"

require seb-image-qt5.inc

IMAGE_FEATURES += " \
    debug-tweaks \
    qtcreator-debug \
    tools-debug \
"
IMAGE_INSTALL += " \
	rsync \
	packagegroup-qt5-qtcreator-debug \
"
