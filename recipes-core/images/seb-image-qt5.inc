inherit core-image

IMAGE_FEATURES += " \
    hwcodecs \
"
inherit populate_sdk_qt5

# Install fonts
QT5_FONTS = "ttf-dejavu-common ttf-dejavu-sans ttf-dejavu-sans-mono ttf-dejavu-serif "

IMAGE_INSTALL += " \
	ca-certificates \
	alsa-utils \
	packagegroup-core-boot \
	qtwebengine \
	qtwebengine-qmlplugins \
	qtquickcontrols \
	qtquickcontrols-qmlplugins \
	qtquickcontrols2 \
	qtquickcontrols2-qmlplugins \
	qtdeclarative \
	qtdeclarative-qmlplugins \
	qtgraphicaleffects \
	qtgraphicaleffects-qmlplugins \
	qtmultimedia \
	qtmultimedia-plugins \
	qtmultimedia-qmlplugins \
	${QT5_FONTS} \
	packagegroup-fsl-gstreamer1.0-full \
	gstreamer1.0-plugins-good-qt \
	qt5-opengles2-test \
	qtmultimedia-examples \
	qtwebengine-examples \
	weston-init \
"
